using Moq.Language;
using Moq.Language.Flow;

namespace CompositeMoq;

public class CompositeCallBase : ICallBase
{
    private readonly IEnumerable<ICallBase> callBases;

    public CompositeCallBase(IEnumerable<ICallBase> callBases)
    {
        this.callBases = callBases.ToList();
    }

    public ICallBaseResult CallBase()
        => new CompositeCallBaseResult(callBases.Select(callBase => callBase.CallBase()));
}