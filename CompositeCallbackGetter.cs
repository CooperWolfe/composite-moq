﻿using Moq;
using Moq.Language;
using Moq.Language.Flow;

namespace CompositeMoq;

public class CompositeCallbackGetter<TClass, TValue> : ICallbackGetter<TClass, TValue>
    where TClass : class
{
    private readonly IEnumerable<ICallbackGetter<TClass, TValue>> callbacks;

    public CompositeCallbackGetter(IEnumerable<ICallbackGetter<TClass, TValue>> callbacks)
    {
        this.callbacks = callbacks.ToList();
    }

    public IReturnsThrowsGetter<TClass, TValue> Callback(Action action)
        => new CompositeReturnsThrowsGetter<TClass, TValue>(callbacks.Select(cb => cb.Callback(action)));
}