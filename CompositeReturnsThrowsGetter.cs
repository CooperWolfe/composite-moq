using Moq.Language;
using Moq.Language.Flow;

namespace CompositeMoq;

public class CompositeReturnsThrowsGetter<TClass, TValue> : IReturnsThrowsGetter<TClass, TValue>
    where TClass : class
{
    private readonly IReturnsGetter<TClass, TValue> returns;
    private readonly IThrows throws;

    public CompositeReturnsThrowsGetter(IEnumerable<IReturnsThrowsGetter<TClass, TValue>> returnsThrowsGetters)
    {
        returns = new CompositeReturnsGetter<TClass, TValue>(returnsThrowsGetters);
        throws = new CompositeThrows(returnsThrowsGetters);
    }

    public IReturnsResult<TClass> CallBase()
        => returns.CallBase();
    public IReturnsResult<TClass> Returns(TValue value)
        => returns.Returns(value);
    public IReturnsResult<TClass> Returns(Func<TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IThrowsResult Throws(Exception exception)
        => throws.Throws(exception);
    public IThrowsResult Throws<TException>() where TException : Exception, new()
        => throws.Throws<TException>();
    public IThrowsResult Throws(Delegate exceptionFunction)
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<TException>(Func<TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T, TException>(Func<T, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, TException>(Func<T1, T2, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, TException>(Func<T1, T2, T3, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, TException>(Func<T1, T2, T3, T4, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, TException>(Func<T1, T2, T3, T4, T5, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, TException>(Func<T1, T2, T3, T4, T5, T6, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, TException>(Func<T1, T2, T3, T4, T5, T6, T7, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
}