﻿using Moq;
using Moq.Language;
using Moq.Language.Flow;

namespace CompositeMoq;

public class CompositeThrowsResult : IThrowsResult
{
    private readonly IOccurrence occurrence;
    private readonly IVerifies verifies;

    public CompositeThrowsResult(IEnumerable<IThrowsResult> throwsResults)
    {
        occurrence = new CompositeOccurrence(throwsResults);
        verifies = new CompositeVerifies(throwsResults);
    }

    [Obsolete]
    public IVerifies AtMost(int callCount)
        => occurrence.AtMost(callCount);
    [Obsolete]
    public IVerifies AtMostOnce()
        => occurrence.AtMostOnce();
    public void Verifiable()
        => verifies.Verifiable();
    public void Verifiable(string failMessage)
        => verifies.Verifiable(failMessage);
    public void Verifiable(Times times)
        => verifies.Verifiable(times);
    public void Verifiable(Func<Times> times)
        => verifies.Verifiable(times);
    public void Verifiable(Times times, string failMessage)
        => verifies.Verifiable(times, failMessage);
    public void Verifiable(Func<Times> times, string failMessage)
        => verifies.Verifiable(times, failMessage);
}
