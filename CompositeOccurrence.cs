﻿using Moq.Language;

namespace CompositeMoq;

public class CompositeOccurrence : IOccurrence
{
    private readonly IEnumerable<IOccurrence> occurrences;

    public CompositeOccurrence(IEnumerable<IOccurrence> occurrences)
    {
        this.occurrences = occurrences.ToList();
    }

    [Obsolete]
    public IVerifies AtMost(int callCount)
        => new CompositeVerifies(occurrences.Select(result => result.AtMost(callCount)));
    [Obsolete]
    public IVerifies AtMostOnce()
        => new CompositeVerifies(occurrences.Select(result => result.AtMostOnce()));
}
