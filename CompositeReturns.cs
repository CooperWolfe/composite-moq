﻿using Moq;
using Moq.Language;
using Moq.Language.Flow;

namespace CompositeMoq;

public class CompositeReturns<TClass, TValue> : IReturns<TClass, TValue>
    where TClass : class
{
    private readonly IEnumerable<IReturns<TClass, TValue>> returns;

    public CompositeReturns(IEnumerable<IReturns<TClass, TValue>> returns)
    {
        this.returns = returns.ToList();
    }

    public IReturnsResult<TClass> CallBase()
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.CallBase()));
    public IReturnsResult<TClass> Returns(TValue value)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(value)));
    public IReturnsResult<TClass> Returns(InvocationFunc valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns(Delegate valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns(Func<TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T>(Func<T, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2>(Func<T1, T2, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2, T3>(Func<T1, T2, T3, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4>(Func<T1, T2, T3, T4, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5>(Func<T1, T2, T3, T4, T5, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6>(Func<T1, T2, T3, T4, T5, T6, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7>(Func<T1, T2, T3, T4, T5, T6, T7, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8>(Func<T1, T2, T3, T4, T5, T6, T7, T8, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(r => r.Returns(valueFunction)));
}
