﻿using Moq;
using Moq.Language;
using Moq.Language.Flow;

namespace CompositeMoq;

public class CompositeSetup<TClass> : ISetup<TClass>
    where TClass : class
{
    private readonly IOccurrence occurrence;
    private readonly ICallback callback;
    private readonly ICallBase callBase;
    private readonly IRaise<TClass> raise;
    private readonly IThrows throws;
    private readonly IVerifies verifies;

    public CompositeSetup(IEnumerable<ISetup<TClass>> setups)
    {
        occurrence = new CompositeOccurrence(setups);
        callback = new CompositeCallback(setups);
        callBase = new CompositeCallBase(setups);
        raise = new CompositeRaise<TClass>(setups);
        throws = new CompositeThrows(setups);
        verifies = new CompositeVerifies(setups);
    }
    public CompositeSetup(params ISetup<TClass>[] setups) : this(setups.AsEnumerable())
    {
    }

    [Obsolete]
    public IVerifies AtMost(int callCount)
        => occurrence.AtMost(callCount);
    [Obsolete]
    public IVerifies AtMostOnce()
        => occurrence.AtMostOnce();
    public ICallbackResult Callback(InvocationAction action)
        => callback.Callback(action);
    public ICallbackResult Callback(Delegate action)
        => callback.Callback(action);
    public ICallbackResult Callback(Action action)
        => callback.Callback(action);
    public ICallbackResult Callback<T>(Action<T> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2>(Action<T1, T2> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3>(Action<T1, T2, T3> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4>(Action<T1, T2, T3, T4> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7>(Action<T1, T2, T3, T4, T5, T6, T7> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8>(Action<T1, T2, T3, T4, T5, T6, T7, T8> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> action)
        => callback.Callback(action);
    public ICallBaseResult CallBase()
        => callBase.CallBase();
    public IVerifies Raises(Action<TClass> eventExpression, EventArgs args)
        => raise.Raises(eventExpression, args);
    public IVerifies Raises(Action<TClass> eventExpression, Func<EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises(Action<TClass> eventExpression, params object[] args)
        => raise.Raises(eventExpression, args);
    public IVerifies Raises<T1>(Action<TClass> eventExpression, Func<T1, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2>(Action<TClass> eventExpression, Func<T1, T2, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3>(Action<TClass> eventExpression, Func<T1, T2, T3, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4>(Action<TClass> eventExpression, Func<T1, T2, T3, T4, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5>(Action<TClass> eventExpression, Func<T1, T2, T3, T4, T5, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6>(Action<TClass> eventExpression, Func<T1, T2, T3, T4, T5, T6, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7>(Action<TClass> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8>(Action<TClass> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Action<TClass> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Action<TClass> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Action<TClass> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(Action<TClass> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(Action<TClass> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(Action<TClass> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(Action<TClass> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(Action<TClass> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IThrowsResult Throws(Exception exception)
        => throws.Throws(exception);
    public IThrowsResult Throws<TException>() where TException : Exception, new()
        => throws.Throws<TException>();
    public IThrowsResult Throws(Delegate exceptionFunction)
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<TException>(Func<TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T, TException>(Func<T, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, TException>(Func<T1, T2, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, TException>(Func<T1, T2, T3, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, TException>(Func<T1, T2, T3, T4, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, TException>(Func<T1, T2, T3, T4, T5, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, TException>(Func<T1, T2, T3, T4, T5, T6, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, TException>(Func<T1, T2, T3, T4, T5, T6, T7, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public void Verifiable()
        => verifies.Verifiable();
    public void Verifiable(string failMessage)
        => verifies.Verifiable(failMessage);
    public void Verifiable(Times times)
        => verifies.Verifiable(times);
    public void Verifiable(Func<Times> times)
        => verifies.Verifiable(times);
    public void Verifiable(Times times, string failMessage)
        => verifies.Verifiable(times, failMessage);
    public void Verifiable(Func<Times> times, string failMessage)
        => verifies.Verifiable(times, failMessage);
}

public class CompositeSetup<TClass, TValue> : ISetup<TClass, TValue>
    where TClass : class
{
    private readonly ICallback<TClass, TValue> callback;
    private readonly IReturns<TClass, TValue> returns;
    private readonly IThrows throws;
    private readonly IVerifies verifies;

    public CompositeSetup(IEnumerable<ISetup<TClass, TValue>> setups)
    {
        callback = new CompositeCallback<TClass, TValue>(setups);
        returns = new CompositeReturns<TClass, TValue>(setups);
        throws = new CompositeThrows(setups);
        verifies = new CompositeVerifies(setups);
    }
    public CompositeSetup(params ISetup<TClass, TValue>[] setups) : this(setups.AsEnumerable())
    {
    }

    public IReturnsThrows<TClass, TValue> Callback(InvocationAction action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback(Delegate @delegate)
        => callback.Callback(@delegate);
    public IReturnsThrows<TClass, TValue> Callback(Action action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T>(Action<T> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2>(Action<T1, T2> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3>(Action<T1, T2, T3> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4>(Action<T1, T2, T3, T4> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7>(Action<T1, T2, T3, T4, T5, T6, T7> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8>(Action<T1, T2, T3, T4, T5, T6, T7, T8> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action)
        => callback.Callback(action);
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> action)
        => callback.Callback(action);
    public IReturnsResult<TClass> CallBase()
        => returns.CallBase();
    public IReturnsResult<TClass> Returns(TValue value)
        => returns.Returns(value);
    public IReturnsResult<TClass> Returns(InvocationFunc valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns(Delegate valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns(Func<TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T>(Func<T, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2>(Func<T1, T2, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3>(Func<T1, T2, T3, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4>(Func<T1, T2, T3, T4, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5>(Func<T1, T2, T3, T4, T5, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6>(Func<T1, T2, T3, T4, T5, T6, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7>(Func<T1, T2, T3, T4, T5, T6, T7, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8>(Func<T1, T2, T3, T4, T5, T6, T7, T8, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IThrowsResult Throws(Exception exception)
        => throws.Throws(exception);
    public IThrowsResult Throws<TException>() where TException : Exception, new()
        => throws.Throws<TException>();
    public IThrowsResult Throws(Delegate exceptionFunction)
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<TException>(Func<TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T, TException>(Func<T, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, TException>(Func<T1, T2, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, TException>(Func<T1, T2, T3, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, TException>(Func<T1, T2, T3, T4, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, TException>(Func<T1, T2, T3, T4, T5, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, TException>(Func<T1, T2, T3, T4, T5, T6, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, TException>(Func<T1, T2, T3, T4, T5, T6, T7, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public void Verifiable()
        => verifies.Verifiable();
    public void Verifiable(string failMessage)
        => verifies.Verifiable(failMessage);
    public void Verifiable(Times times)
        => verifies.Verifiable(times);
    public void Verifiable(Func<Times> times)
        => verifies.Verifiable(times);
    public void Verifiable(Times times, string failMessage)
        => verifies.Verifiable(times, failMessage);
    public void Verifiable(Func<Times> times, string failMessage)
        => verifies.Verifiable(times, failMessage);
}
