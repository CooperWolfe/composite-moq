﻿using Moq;
using Moq.Language;
using Moq.Language.Flow;

namespace CompositeMoq;

public class CompositeReturnsResult<T> : IReturnsResult<T>
{
    private readonly IOccurrence occurrence;
    private readonly ICallback callback;
    private readonly IRaise<T> raise;
    private readonly IVerifies verifies;

    public CompositeReturnsResult(IEnumerable<IReturnsResult<T>> returnsResults)
    {
        occurrence = new CompositeOccurrence(returnsResults);
        callback = new CompositeCallback(returnsResults);
        raise = new CompositeRaise<T>(returnsResults);
        verifies = new CompositeVerifies(returnsResults);
    }

    [Obsolete]
    public IVerifies AtMost(int callCount)
        => occurrence.AtMost(callCount);
    [Obsolete]
    public IVerifies AtMostOnce()
        => occurrence.AtMostOnce();
    public ICallbackResult Callback(InvocationAction action)
        => callback.Callback(action);
    public ICallbackResult Callback(Delegate @delegate)
        => callback.Callback(@delegate);
    public ICallbackResult Callback(Action action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1>(Action<T1> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2>(Action<T1, T2> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3>(Action<T1, T2, T3> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4>(Action<T1, T2, T3, T4> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6> action)
        => callback.Callback(action); 
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7>(Action<T1, T2, T3, T4, T5, T6, T7> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8>(Action<T1, T2, T3, T4, T5, T6, T7, T8> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action)
        => callback.Callback(action);
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> action)
        => callback.Callback(action);
    public IVerifies Raises(Action<T> eventExpression, EventArgs args)
        => raise.Raises(eventExpression, args);
    public IVerifies Raises(Action<T> eventExpression, Func<EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises(Action<T> eventExpression, params object[] args)
        => raise.Raises(eventExpression, args);
    public IVerifies Raises<T1>(Action<T> eventExpression, Func<T1, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2>(Action<T> eventExpression, Func<T1, T2, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3>(Action<T> eventExpression, Func<T1, T2, T3, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4>(Action<T> eventExpression, Func<T1, T2, T3, T4, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, EventArgs> func)
        => raise.Raises(eventExpression, func);
    public void Verifiable()
        => verifies.Verifiable();
    public void Verifiable(string failMessage)
        => verifies.Verifiable(failMessage);
    public void Verifiable(Times times)
        => verifies.Verifiable(times);
    public void Verifiable(Func<Times> times)
        => verifies.Verifiable(times);
    public void Verifiable(Times times, string failMessage)
        => verifies.Verifiable(times, failMessage);
    public void Verifiable(Func<Times> times, string failMessage)
        => verifies.Verifiable(times, failMessage);
}
