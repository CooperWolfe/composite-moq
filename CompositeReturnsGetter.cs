using Moq.Language;
using Moq.Language.Flow;

namespace CompositeMoq;

public class CompositeReturnsGetter<TClass, TValue> : IReturnsGetter<TClass, TValue>
    where TClass : class
{
    private readonly IEnumerable<IReturnsGetter<TClass, TValue>> returns;

    public CompositeReturnsGetter(IEnumerable<IReturnsGetter<TClass, TValue>> returns)
    {
        this.returns = returns.ToList();
    }

    public IReturnsResult<TClass> CallBase()
        => new CompositeReturnsResult<TClass>(returns.Select(ret => ret.CallBase()));
    public IReturnsResult<TClass> Returns(TValue value)
        => new CompositeReturnsResult<TClass>(returns.Select(ret => ret.Returns(value)));
    public IReturnsResult<TClass> Returns(Func<TValue> valueFunction)
        => new CompositeReturnsResult<TClass>(returns.Select(ret => ret.Returns(valueFunction)));
}