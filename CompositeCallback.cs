﻿using Moq;
using Moq.Language;
using Moq.Language.Flow;

namespace CompositeMoq;

public class CompositeCallback : ICallback
{
    private readonly IEnumerable<ICallback> callbacks;

    public CompositeCallback(IEnumerable<ICallback> callbacks)
    {
        this.callbacks = callbacks.ToList();
    }

    public ICallbackResult Callback(InvocationAction action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback(Delegate @delegate)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(@delegate)));
    public ICallbackResult Callback(Action action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T>(Action<T> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2>(Action<T1, T2> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2, T3>(Action<T1, T2, T3> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2, T3, T4>(Action<T1, T2, T3, T4> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7>(Action<T1, T2, T3, T4, T5, T6, T7> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8>(Action<T1, T2, T3, T4, T5, T6, T7, T8> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
    public ICallbackResult Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> action)
        => new CompositeCallbackResult(callbacks.Select(callback => callback.Callback(action)));
}
public class CompositeCallback<TClass, TValue> : ICallback<TClass, TValue>
    where TClass : class
{
    private readonly IEnumerable<ICallback<TClass, TValue>> callbacks;

    public CompositeCallback(IEnumerable<ICallback<TClass, TValue>> callbacks)
    {
        this.callbacks = callbacks;
    }

    public IReturnsThrows<TClass, TValue> Callback(InvocationAction action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback(Delegate @delegate)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(@delegate)));
    public IReturnsThrows<TClass, TValue> Callback(Action action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T>(Action<T> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2>(Action<T1, T2> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3>(Action<T1, T2, T3> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4>(Action<T1, T2, T3, T4> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7>(Action<T1, T2, T3, T4, T5, T6, T7> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8>(Action<T1, T2, T3, T4, T5, T6, T7, T8> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
    public IReturnsThrows<TClass, TValue> Callback<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> action)
        => new CompositeReturnsThrows<TClass, TValue>(callbacks.Select(callback => callback.Callback(action)));
}