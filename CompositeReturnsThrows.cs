﻿using Moq;
using Moq.Language;
using Moq.Language.Flow;

namespace CompositeMoq;

public class CompositeReturnsThrows<TClass, TValue> : IReturnsThrows<TClass, TValue>
    where TClass : class
{
    private readonly IReturns<TClass, TValue> returns;
    private readonly IThrows throws;

    public CompositeReturnsThrows(IEnumerable<IReturnsThrows<TClass, TValue>> returnsThrows)
    {
        returns = new CompositeReturns<TClass, TValue>(returnsThrows);
        throws = new CompositeThrows(returnsThrows);
    }

    public IReturnsResult<TClass> CallBase()
        => returns.CallBase();
    public IReturnsResult<TClass> Returns(TValue value)
        => returns.Returns(value);
    public IReturnsResult<TClass> Returns(InvocationFunc valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns(Delegate valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns(Func<TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T>(Func<T, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2>(Func<T1, T2, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3>(Func<T1, T2, T3, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4>(Func<T1, T2, T3, T4, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5>(Func<T1, T2, T3, T4, T5, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6>(Func<T1, T2, T3, T4, T5, T6, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7>(Func<T1, T2, T3, T4, T5, T6, T7, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8>(Func<T1, T2, T3, T4, T5, T6, T7, T8, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IReturnsResult<TClass> Returns<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TValue> valueFunction)
        => returns.Returns(valueFunction);
    public IThrowsResult Throws(Exception exception)
        => throws.Throws(exception);
    public IThrowsResult Throws<TException>() where TException : Exception, new()
        => throws.Throws<TException>();
    public IThrowsResult Throws(Delegate exceptionFunction)
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<TException>(Func<TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T, TException>(Func<T, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, TException>(Func<T1, T2, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, TException>(Func<T1, T2, T3, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, TException>(Func<T1, T2, T3, T4, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, TException>(Func<T1, T2, T3, T4, T5, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, TException>(Func<T1, T2, T3, T4, T5, T6, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, TException>(Func<T1, T2, T3, T4, T5, T6, T7, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
    public IThrowsResult Throws<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TException>(Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TException> exceptionFunction) where TException : Exception
        => throws.Throws(exceptionFunction);
}
