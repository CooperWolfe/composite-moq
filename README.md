# Composite Moq
This project implements the Moq library's language flow interfaces using the composite pattern. This empowers developers to setup and verify multiple calls at once: particularly helpful for certain method overloads.

## Usage
```csharp
var setup = new CompositeSetup<IInterface, string>(
    mock.Setup(iface => iface.Call()),
    mock.Setup(iface => iface.Call(It.IsAny<CancellationToken>())));
setup.Returns("Hello!");

string message1 = mock.Object.Call();
string message2 = mock.Object.Call(default);

Assert.Equal("Hello!", message1);
Assert.Equal("Hello!", message2);
```
