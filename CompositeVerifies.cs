﻿using Moq;
using Moq.Language;

namespace CompositeMoq;

public class CompositeVerifies : IVerifies
{
    private readonly IEnumerable<IVerifies> verifies;

    public CompositeVerifies(IEnumerable<IVerifies> verifies)
    {
        this.verifies = verifies.ToList();
    }

    public void Verifiable()
    {
        foreach (var verifies in this.verifies)
        {
            verifies.Verifiable();
        }
    }

    public void Verifiable(string failMessage)
    {
        foreach (var verifies in this.verifies)
        {
            verifies.Verifiable(failMessage);
        }
    }

    public void Verifiable(Times times)
    {
        foreach (var verifies in this.verifies)
        {
            verifies.Verifiable(times);
        }
    }

    public void Verifiable(Func<Times> times)
    {
        foreach (var verifies in this.verifies)
        {
            verifies.Verifiable(times);
        }
    }

    public void Verifiable(Times times, string failMessage)
    {
        foreach (var verifies in this.verifies)
        {
            verifies.Verifiable(times, failMessage);
        }
    }

    public void Verifiable(Func<Times> times, string failMessage)
    {
        foreach (var verifies in this.verifies)
        {
            verifies.Verifiable(times, failMessage);
        }
    }
}
