﻿using Moq.Language;

namespace CompositeMoq;

public class CompositeRaise<T> : IRaise<T>
{
    private readonly IEnumerable<IRaise<T>> raises;

    public CompositeRaise(IEnumerable<IRaise<T>> raises)
    {
        this.raises = raises.ToList();
    }

    public IVerifies Raises(Action<T> eventExpression, EventArgs args)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, args)));
    public IVerifies Raises(Action<T> eventExpression, Func<EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises(Action<T> eventExpression, params object[] args)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, args)));
    public IVerifies Raises<T1>(Action<T> eventExpression, Func<T1, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2>(Action<T> eventExpression, Func<T1, T2, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2, T3>(Action<T> eventExpression, Func<T1, T2, T3, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2, T3, T4>(Action<T> eventExpression, Func<T1, T2, T3, T4, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2, T3, T4, T5>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2, T3, T4, T5, T6>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
    public IVerifies Raises<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(Action<T> eventExpression, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, EventArgs> func)
        => new CompositeVerifies(raises.Select(raise => raise.Raises(eventExpression, func)));
}
